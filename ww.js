const express = require('express');
const app = express();

const { proxy, scriptUrl } = require('rtsp-relay')(app);


app.ws('/api/stream', proxy({
  url: `rtsp://94.182.204.102:8554/octovision`,
  verbose: false,
  additionalFlags: ['-q', '1'] 
}));

app.get('/', (req, res) =>
  res.send(`
  <body style='padding:0; margin: 0;'>
     <canvas id='canvas' style='width: 100vw; height: 100vh;'></canvas>
  </body>
  <script src='${scriptUrl}'></script>
  <script>
    loadPlayer({
      url: 'ws://' + location.host + '/api/stream',
      canvas: document.getElementById('canvas')
    });
  </script>
`),
);

app.listen(2002);
console.info("app is running on http://localhost:2002");